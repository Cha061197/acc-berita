<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey='id';
    public $timestamps = true;
    
    protected $guarded = [
        'created_at',
        'updated_at',
    ];

    public function blogs(){
        return $this->hasMany(Blog::class,'category_id','id');
    }
}
