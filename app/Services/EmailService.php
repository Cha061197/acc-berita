<?php

namespace App\Services;

use App\Blog;
use App\User;
use Illuminate\Support\Facades\Mail;

class EmailService
{
    /**
     * Send Email to author user that the blog has been rejected
     * @param $id [id of accepted blog]
     * @return void
     */
    public function sendEmailForRejectedBlog($id){
        $Blog = new Blog;
        $User = new User;

        $blog = $Blog->find($id);
        $author = $User->where('id',$blog->user_id)->first();

        $messageContetn = "Kepada ".$author->nama." \n Kami telah menerima Blog yang anda buat dengan judul"
        . $blog->title.", kami sampaikan bahwa Blog anda masih belum bisa kami terima, silahkan perbaiki kembali.";

        Mail::raw($messageContetn, function ($message) use ($author){
            $message->to($author->email)
                ->subject('Blog Rejected');
        });
    }

    public function sendEmailForAcceptedBlog($id){
        $Blog = new Blog;
        $User = new User;

        $blog = $Blog->find($id);
        $author = $User->where('id',$blog->user_id)->first();

        $messageContetn = "Kepada ".$author->nama." \n Kami telah menerima Blog yang anda buat dengan judul"
        . $blog->title.", kami sampaikan bahwa Blog sudah berstatus diterima, kunjungi di ".Route('blog.show',['id'=>$blog->id]);

        Mail::raw($messageContetn, function ($message) use ($author){
            $message->to($author->email)
                ->subject('Blog Accepted');
        });
    }
}