<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Blog;
use App\User;
use App\Category;
use App\Services\EmailService;
use App\Services\BlogService;
use App\Http\Requests\BlogStoreRequest;
use App\Http\Requests\BlogUpdateRequest;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['show']]);
        $this->middleware('verified',['except' => ['show','dashboard','reject','accept']]);
        $this->middleware('adminUser', ['only' => ['dashboard','reject','accept']]);
        $this->middleware('authorUser', ['only' => ['index','edit','create','update','store','delete']]);
    }
    
    public function index(Request $request){
        $Blog = new Blog;
        $user_id = Auth::user()->id;

        $blogs = $Blog->where('user_id',$user_id)->paginate(10);

        return view('blog.index',compact('blogs'));
    }

    public function create(){
        $Category = new Category;

        $categories = $Category->get();

        return view('blog.create',compact('categories'));
    }

    public function store(BlogStoreRequest $request){
        $Blog = new Blog;
        $Blog->create([
            'title' => $request->title,
            'content' => $request->content,
            'user_id' => Auth::user()->id,
            'category_id' => $request->category_id,
            'status' => 1,
            'flag_active' => 1
        ]);

        return redirect()->route('blog.index')->with(['success'=>'Berhasil Menambahkan Blog']);
    }

    public function edit(Request $request,$id){
        $Blog = new Blog;
        $Category = new Category;

        $blog = $Blog->whereId($id)->first();
        $categories = $Category->get();

        if($blog != null && $blog->user_id == Auth::user()->id){
            return view('blog.edit',compact('blog','categories'));
        }else{
            return redirect()->route('blog.index');
        }
    }

    public function update(BlogUpdateRequest $request){
        $Blog = new Blog;
        $blogWillBeUpdate = $Blog->find($request->id);

        if($blogWillBeUpdate != null && $blogWillBeUpdate->user_id == Auth::user()->id){
            $Blog->whereId($request->id)
                ->update([
                    'title' => $request->title,
                    'content' => $request->content,
                    'status' => 1,
                    'category_id' => $request->category_id,
                    'flag_active' => $request->flag_active
                ]);
            return redirect()->route('blog.index')->with(['success' => "Berhasil mengupdate blog"]);
        }else{
            return redirect()->route('blog.index')->with(['error' => "Terjadi kesalahan sistem"]);
        }
    }

    public function delete(Request $request){
        $Blog = new Blog;

        $blogWillBeDelete = $Blog->find($request->id);

        if($blogWillBeDelete != null && $blogWillBeDelete->user_id == Auth::user()->id){
            $Blog->whereId($request->id)->delete();
            return redirect()->route('blog.index')->with(['success' => "Berhasil Menghapus Blog"]);
        }else{
            return redirect()->route('blog.index')->with(['error' => "Terjadi kesalahan sistem"]);
        }
    }

    public function show($id){
        $Blog = new Blog;
        $blog = $Blog->whereId($id)->with('user')->first();

        Auth::check() ? $user_id = Auth::user()->id : $user_id = 0;

        return view('blog.show',compact('blog','user_id'));
    }

    public function dashboard(){
        $Blog = new Blog;
        $blogs = $Blog->with('user')->paginate(20);

        return view('blog.dashboard',compact('blogs'));
    }

    public function accept(Request $request,$id){
        $EmailService = new EmailService;
        $BlogService = new BlogService;

        if($BlogService->blogIsExist($request->id)){
            $BlogService->updateBlogStatusToAccepted($request->id);
            $EmailService->sendEmailForAcceptedBlog($request->id);
            
            return redirect()->back()->with(['success' => 'Mengubah status Blog dengan id '.$id.' menjadi accepted']);
        }else{
            return redirect()->back()->with(['error' => 'Terjadi kesalahan sistem']);
        }
    }

    public function reject(Request $request,$id){
        $EmailService = new EmailService;
        $BlogService = new BlogService;

        if($BlogService->blogIsExist($request->id)){
            $BlogService->updateBlogStatusToRejected($request->id);
            $EmailService->sendEmailForRejectedBlog($request->id);

            return redirect()->back()->with(['success' => 'Mengubah status Blog dengan id '.$id.' menjadi rejected']);
        }else{
            return redirect()->back()->with(['error' => 'Terjadi kesalahan sistem']);
        }
    }
}
