<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('adminUser');
    }

    public function index(){
        $Category = new Category;

        $categories = $Category->get();

        return view('category.index',compact('categories'));
    }

    public function create(){
        return view('category.create');
    }

    public function store(Request $request){
        $Category = new Category;
        $Category->create([
            'name' => $request->name,
        ]);

        return redirect()->route('category.index')->with(['success' => 'Berhasil menambahkan category']);
    }

    public function edit($id){
        $Category = new Category;

        $category = $Category->find($id);

        return view('category.edit',compact('category'));
    }

    public function update(Request $request){
        $Category = new Category;
        $Category->whereId($request->id)
            ->update([
                'name' => $request->name,
            ]);

        return redirect()->route('category.index')->with(['success' => 'Berhasil menambahkan category']);
    }
}
