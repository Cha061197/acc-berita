<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function home(){
        return view('home');
    }

    public function blog(){
        $Blog = new Blog;
        $Category = new Category;

        $blogs = $Blog
            ->with('user')
            ->where('status',3)
            ->where('flag_active',1)
            ->paginate(10);
        $categories = $Category->get();
        
        return view('blog',compact('blogs','categories'));
    }

    public function blogWithCategory($id)
    {
        $Blog = new Blog;
        $Category = new Category;

        $blogs = $Blog
            ->with('user','category')
            ->where('category_id',$id)
            ->paginate(10);

        $categories = $Category->get();
        
        return view('blog',compact('blogs','categories'));
    }

    public function aboutUs(){
        return view('aboutUs');
    }
}
