<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class BlogUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'title' => 'required|max:255',
            'content' => 'required',
            'flag_active' => 'required|in:1,0'
        ];
    }

    public function messages(){
        return [
            'id.required' => 'Terjadi masalah',
            'title.required' => 'Judul tidak boleh kosong',
            'title.max' => 'Panjang karakter maksimal pada judul adalah 255',
            'content.required' => 'kontent tidak boleh kosong',
            'flag_active.required' => 'status aktif blog wajib di isi',
            'flag_active.in' => 'status aktif blog wajib di isi'
        ];
    }
}
