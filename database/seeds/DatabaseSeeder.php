<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(users_table_seeder::class);
        // $this->call(blog_table_seeder::class);
        $this->call(category_table_seeder::class);
    }
}
