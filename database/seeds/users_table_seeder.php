<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class users_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('Password@_1'),
            'created_at' => '2021-01-01 00:00:00',
            'updated_at' => '2021-01-01 00:00:00',
            'role' => '1'
        ]);

        for ($i=2; $i <= 50; $i++) { 
            DB::table('users')->insert([
                'id' => $i,
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('Password@_1'),
                'created_at' => '2021-01-01 00:00:00',
                'updated_at' => '2021-01-01 00:00:00',
                'role' => '2'
            ]);
        }
    }
}
