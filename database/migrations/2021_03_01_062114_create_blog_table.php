<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->notNullAble();
            $table->text('content')->notNullAble();
            $table->bigInteger('user_id')->unsigned()->notNullAble();
            $table->integer('status')->length(1)->nullAble()->default(0);
            $table->dateTime('accepted_at')->nullAble();
            $table->boolean('flag_active')->notNullAble()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
