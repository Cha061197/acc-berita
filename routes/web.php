<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@home')->name('home');
Route::get('/about-us', 'HomeController@aboutUs')->name('aboutUs');
Route::get('/blog', 'HomeController@blog')->name('blog');
Route::get('/blog/category/{id}', 'HomeController@blogWithCategory')->name('blog.category');

Route::get('/myBlog','BlogController@index')->name('blog.index');
Route::get("/blog/create","BlogController@create")->name("blog.create");
Route::post("/blog","BlogController@store")->name("blog.store");
Route::get("/blog/{id}/edit","BlogController@edit")->name('blog.edit');
Route::get("/blog/{id}/show","BlogController@show")->name('blog.show');
Route::put("/blog","BlogController@update")->name('blog.update');
Route::delete('/blog','BlogController@delete')->name('blog.delete');
Route::get('/dashboard',"BlogController@dashboard")->name('blog.dashboard');
Route::post('/blog/{id}/reject','BlogController@reject')->name('blog.reject');
Route::post('/blog/{id}/accept','BlogController@accept')->name('blog.accept');

Route::get('/category','CategoryController@index')->name('category.index');
Route::get('/category/create','CategoryController@create')->name('category.create');
Route::get('/category/{id}/edit','CategoryController@edit')->name('category.edit');
Route::post('/category','CategoryController@store')->name('category.store');
Route::put('/category','CategoryController@update')->name('category.update');
