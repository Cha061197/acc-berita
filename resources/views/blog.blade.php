@extends('layouts/app')
@section('title')
    Blog
@endsection
@section('content')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Halaman Blog</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach ($categories as $category)
                <form action="{{route('blog.category',['id'=>$category->id])}}" method="get">
                    <button class="ml-2 btn btn-primary" type="submit">{{$category->name}}</button>
                </form>
            @endforeach
        </div>
        <div class="row">
            @foreach ($blogs as $blog)
                <div class="col-md-6 mt-2">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{$blog->title}} 
                                @if ($blog->category)
                                    - <b>{{$blog->category->name}}</b>
                                @endif
                            </h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$blog->user->name}}</h6>
                            <a href="{{route('blog.show',['id' => $blog->id])}}" class="card-link">read more</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mt-5">
            {{ $blogs->links( "pagination::bootstrap-4") }}
        </div>
    </div>
@endsection